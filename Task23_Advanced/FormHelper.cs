﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task23_Advanced.Helpers
{
    public static class FormHelper
    {
        public static MvcHtmlString CreateRadioList(this HtmlHelper html, IEnumerable<string> items, string name)
        {
            TagBuilder ul = new TagBuilder("ul");
            foreach (var item in items)
            {
                var li = new TagBuilder("li");
                var input = new TagBuilder("input");
                input.MergeAttributes(new Dictionary<string, string> { { "type", "radio" }, { "name", name }, { "value", item } });
                li.InnerHtml += input.ToString();
                var p = new TagBuilder("p");
                p.InnerHtml += item;
                li.InnerHtml += p.ToString();
                ul.InnerHtml += li.ToString();
            }
            return new MvcHtmlString(ul.ToString());
        }
    }
}