﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task23_Advanced.Controllers
{
    public class FormController : Controller
    {
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Form(FormCollection form)
        {
            var result = new List<(string field, string value)>();
            string str;
            for (int i = 0; i < form.Count; i++)
            {
                str = form.GetKey(i).ToString();
                result.Add((str, form.Get(str).ToString()));
            }
            TempData["list"] = result;
            return RedirectToAction("FormResult");
        }
        public ActionResult FormResult()
        {
            ViewBag.Result = TempData["list"] as List<(string field, string value)>;
            return View();
        }
    }
}